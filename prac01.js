// q1
var pi, r, result;
pi = Math.PI;
r = 4;
result = 2*pi*r; 
console.log(result.toFixed(2))

//q2
let animalString = "cameldogcatlizard";
let andString = " and ";
console.log(animalString.substr(8,3) + andString + animalString.substr(5,3))
console.log(animalString.substring(8,11) + andString + animalString.substring(5,8))

//q3
var FirstName,LastName,BirthDate,AnnualIncome
FirstName = "Kanye"
LastName = "West"
BirthDate = "8 June 1977"
AnnualIncome = 150000000.00
console.log(FirstName + LastName +  "was born on" +  BirthDate +  "and has an annual income of $" + AnnualIncome.toString())

//q4
var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
//HERE your code to swap the values in number1 and number2
console.log("number1 = " + number2 + " number2 = " + number1);

//q5
let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);